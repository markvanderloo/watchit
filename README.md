# watchit

scripts to be run with CRON, to watch how rev deps grow over time.

To start following your package:

1. Use a computer that's always on and connected to the internet.
2. In a bash shell, clone this repo

```bash
git clone https://gitlab.com/markvanderloo/watchit
cd watchit
```

3. To schedule a job in crontab:

```bash
# edit your crontab
crontab -e

# add a line like this for a daily update at 4 AM
0 4 * * * cd /home/mark/projects/watchit && ./get_revdeps.R tinytest
0 4 * * * cd /home/mark/projects/watchit && ./plot_revdeps.R tinytest suggests
```

The script `get_revdeps.R` does the following.

1. Download the CRAN database
2. Extract and count dependencies
3. Create a backup of `data/PACKAGE.csv` and
4. Add a line to `data/PACKAGE.csv`

A line is written to `log/log.txt` in case of an error (warnings are currently ignored)



The script `plot_revdeps.R` does the following.

1. Read `data/PACKAGE.csv`
2. Create, and generally overwrites `plots/PACKAGE_DEPENDECYTYPE.pkg`.





