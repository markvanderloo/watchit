#! /usr/bin/Rscript

logfile <- "log/log.txt"
L <- commandArgs(trailingOnly=TRUE)
pkg = L[[1]]


tryCatch({

  db <- tools::CRAN_package_db()

  i <- db$Package == pkg

  depends   <- db[i,"Reverse depends"]
  imports   <- db[i,"Reverse imports"]
  linkingto <- db[i,"Reverse linking to"]
  suggests  <- db[i,"Reverse suggests"]
  enhances  <- db[i,"Reverse enhances"]

  n <- function(x) if(is.na(x)) 0 else length(strsplit(x,", ")[[1]])


  now <-  format(Sys.time(), "%Y-%m-%d")

  out <- data.frame(
        date = now
      , depends = n(depends)
      , imports = n(imports)
      , linkingto = n(linkingto)
      , suggests  = n(suggests)
      , enhances  = n(enhances) 
    ) 

  outfile <- file.path("data", sprintf("%s.csv",pkg))
  if (!file.exists(outfile)){
    write.csv(out, outfile, row.names=FALSE, quote=FALSE)
  } else {
    backup <- sprintf("backup/%s_%s_backup.csv",now,pkg)
    file.copy(outfile, backup)
    write.table(out
      , file=outfile
      , sep=","
      , quote=FALSE
      , row.names=FALSE
      , append=TRUE
      , col.names=FALSE)
  }

}
  , error = function(e){
     msg <- sprintf("%s: [%s|ERROR]: %s", as.character(date()), pkg ,e$message)
     write(msg, file=logfile, append=TRUE)
  }

)



